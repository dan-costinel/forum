<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ThreadsController extends Controller
{
    /**
     * @Route("/thread/{id}", name="app_thread_route", requirements={"id": "\d+"})
     */
    public function thread1Action($id)
    {
//        if (!$id){
//            return $this->generateUrl('_twig_error_test');
//        }
        return $this->render('AppBundle:Threads:thread.html.twig', ['id'=>$id]);
    }
}
