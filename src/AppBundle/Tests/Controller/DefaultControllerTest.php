<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testCompleteScenario()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode()); // 200 OK ??
        $this->assertTrue($client->getResponse()->isSuccessful()); // 200 OK ??
        $this->assertContains('<a class="navbar-brand" href="/">My Forum</a>', $client->getResponse()->getContent());
        $this->assertContains('<li><a href="/register/">Register</a></li>', $client->getResponse()->getContent());
        $this->assertContains('<li><a href="/login">Login</a></li>', $client->getResponse()->getContent());
        $this->assertContains('<div class="container-fluid content">', $client->getResponse()->getContent());

        $crawler = $client->request('GET', '/register/');
        $this->assertTrue($client->getResponse()->isSuccessful()); // 200 OK ??

        $crawler = $client->request('GET', '/login');
        $this->assertTrue($client->getResponse()->isSuccessful()); // 200 OK ??

//        $client = static::createClient(array(), array(
//            'PHP_AUTH_USER' => 'admin',
//            'PHP_AUTH_PW'   => 'admin',
//        ));
//        $client->request('GET', '/admin/');
//        $client->followRedirects(true);
//        $this->assertTrue($client->getResponse()->isRedirect());
//        $this->assertContains('Add User', $client->getResponse()->getContent());


    }
}
