<?php
use \WebGuy;

class TestCest
{
    private $admin_user = 'admin';
    private $admin_pass = 'admin';

    private $user_user = 'user1';
    private $pass_user = 'user';

    private $fail_user = 'failuser';
    private $fail_pass = 'failpass';

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    private function failLoginAsAdmin(WebGuy $I)
    {
        $I->wantTo("Fail the login as an admin");
        $I->amOnPage('/login');
        $I->submitForm('#login_form', array(
            '_username' => $this->fail_user,
            '_password' => $this->fail_pass,
            'submit' => '_submit'
        ));
        $I->seeCurrentUrlEquals('/app_dev.php/login');
        $I->see('Invalid credentials.');
    }

    private function succedLoginAsAdmin(WebGuy $I)
    {
        # OBS sometimes the admin login is problematic in the sens that if he is in /admin and then access in the
        #    browser /logout, then when he re-logins the test will fail, because the expected url is / and the
        #    current is /admin/....

        $I->wantTo("Succed the login as an admin");
        $I->amOnPage('/login');
        $I->submitForm('#login_form', array(
            '_username' => $this->admin_user,
            '_password' => $this->admin_pass,
            'submit' => '_submit'
        ));
        $I->seeCurrentUrlEquals('/app_dev.php/');
    }

    private function whatTextContainsHomepageWhenLoggedInAsAdmin(WebGuy $I)
    {
        $I->wantTo('See the texts inside homepage, when logged in as an admin');
        $I->amOnPage('/');
        $I->seeCurrentUrlEquals('/app_dev.php/');
        $I->see('My Forum');
        $I->see('Hello, admin');
        $I->see('Admin');
        $I->see('Profile');
        $I->see('Logout');
        $I->see('HOME PAGE');
        $I->see('Site map');
        $I->see('Latest threads');
    }

    private function testLinksFromHomepageWhenLoggedInAsAdmin(WebGuy $I)
    {
        $I->wantTo('Test all links from homepage when logged in as an admin');
//        $I->amOnPage('/app_dev.php/');
        $I->amOnPage('/');

        $I->click('a.navbar-brand');
        $I->seeCurrentUrlEquals('/app_dev.php/');

        $I->click('a.nofollow');
        $I->seeCurrentUrlEquals('/app_dev.php/');

        $I->click('Admin');
        $I->seeCurrentUrlEquals('/app_dev.php/admin/?action=list&entity=User');

        $I->amOnPage('/');
        $I->seeCurrentUrlEquals('/app_dev.php/');
        $I->click('Profile');
        $I->seeCurrentUrlEquals('/app_dev.php/profile/');

        //more to follow....
    }

    private function testAdminPage(WebGuy $I)
    {
        $I->amOnPage('/admin');
        $I->see('EasyAdmin');
        $I->see('admin');
        $I->see('User');
        $I->see('Search');
        $I->see('Add User');
        $I->see('Username');
        $I->see('Username canonical');
        $I->see('Email');
        $I->see('Email canonical');
        $I->see('Enabled');
        $I->see('Last login');
        $I->see('Locked');
        $I->see('Actions');
        $I->see('user1');
        $I->see('user@user.com');
//        $I->see('Yes');
//        $I->see('March 26, 2016 19:34'); //se modifica dupa fiecare test
//        $I->see('No');
        $I->see('Edit');
        $I->see('Delete');
    }

    /**
     ******************************************************************************************************
     */

    private function failLoginAsUser(WebGuy $I)
    {
        $I->wantTo("Fail the login as a regular user");
        $I->amOnPage('/login');
        $I->submitForm('#login_form', array(
            '_username' => $this->fail_user,
            '_password' => $this->fail_pass,
            'submit' => '_submit'
        ));
        $I->seeCurrentUrlEquals('/app_dev.php/login');
        $I->see('Invalid credentials.');
    }

    private function succesLoginAsUser(WebGuy $I)
    {
        $I->wantTo("Succed the login as regular user");
        $I->amOnPage('/login');
        $I->submitForm('#login_form', array(
            '_username' => $this->user_user,
            '_password' => $this->pass_user,
            'submit' => '_submit'
        ));
        $I->seeCurrentUrlEquals('/app_dev.php/');
    }

    private function whatTextContainsHomepageWhenLoggedInAsUser(WebGuy $I)
    {
        $I->amOnPage('/app_dev.php/');
        $I->seeCurrentUrlEquals('/app_dev.php/');
        $I->see('My Forum');
        $I->see('Hello, admin');
        $I->see('Profile');
        $I->see('Logout');
        $I->see('HOME PAGE');
        $I->see('Site map');
        $I->see('Latest threads');
    }

    private function testLinksFromHomepageWhenLoggedInAsUser(WebGuy $I)
    {
        $I->amOnPage('/app_dev.php/');

        $I->click('a.navbar-brand');
        $I->seeCurrentUrlEquals('/app_dev.php/');

        $I->click('a.nofollow');
        $I->seeCurrentUrlEquals('/app_dev.php/');

        $I->click('Profile');
        $I->seeCurrentUrlEquals('/app_dev.php/profile/');

        //....more to follow
    }

    /**
     ******************************************************************************************************
     */
    private function testLinksFromHomepage(WebGuy $I)
    {
        $I->amOnPage('/');
        $I->seeCurrentUrlEquals('/app_dev.php/');
        $I->see('My Forum');
        $I->see('Login');
        $I->see('Register');
        $I->see('Lost Password?');
        $I->see('HOME PAGE');
        $I->see('Site map');
        $I->see('Latest threads');
    }

    private function testRegisterPage(WebGuy $I)
    {
        $I->amOnPage('/register/');
        $I->see('Password');
        $I->see('Repeat password');
        $I->seeCurrentUrlEquals('/app_dev.php/register/');
        $I->click('Register');
//        $I->see('Please fill out this field.'); REQUIRES JAVASCRIPT TESTING
    }

    private function testLoginPage(WebGuy $I)
    {
        $I->amOnPage("/login");
        $I->seeCurrentUrlEquals("/app_dev.php/login");
        $I->see('My Forum');
        $I->see('Home');
        $I->see('Register');
        $I->see('LOGIN');
        $I->see('Username');
        $I->see('Password');
        $I->see('Remember me');
//        $I->see('Log in'); REQUIRES JAVASCRIPT?? BEING A BUTTON?
        $I->see('Forgot password?');
        $I->see('Site map');
        $I->see('Latest threads');

        # Testing the login when filling the form and submitting it, is tested in testAsAdmin() method
    }

    //main method
    public function completeScenario(WebGuy $I)
    {
        $this->failLoginAsAdmin($I);
//        $this->succedLoginAsAdmin($I);
//        $this->whatTextContainsHomepageWhenLoggedInAsAdmin($I);
//        $this->testLinksFromHomepageWhenLoggedInAsAdmin($I);

        # ################################################################################

//        $this->failLoginAsUser($I);
//        $this->succesLoginAsUser($I);

        # ################################################################################

        $this->testLinksFromHomepage($I);
    }
}
